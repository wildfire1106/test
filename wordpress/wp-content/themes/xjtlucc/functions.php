<?php

//theme constants
define( 'THEME_NAME', 'library' );

//uri constants
define( 'THEME_URI', get_stylesheet_directory_uri() . /);
define( 'THEME_ASSETS_URI', THEME_URI . 'assets' . /);
define( 'THEME_STYLESHEETS_URI', THEME_ASSETS_URI . 'css' . /);
define( 'THEME_JAVASCRIPTS_URI', THEME_ASSETS_URI . 'js' . /);

//dir constants
define( 'THEME_DIR', get_stylesheet_directory() . /);
define( 'THEME_INCLUDES_DIR', THEME_DIR . 'includes' . /);
define( 'THEME_ASSETS_DIR', THEME_DIR . 'assets' . /);
define( 'THEME_STYLESHEETS_DIR', THEME_ASSETS_DIR . 'css' . /);
define( 'THEME_JAVASCRIPTS_DIR', THEME_ASSETS_DIR . 'js' . /);

//functions
require_once THEME_INCLUDES_DIR . 'functions-xjtlucc-setup.php';
require_once THEME_INCLUDES_DIR . 'functions-xjtlucc-assets.php';