<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><?php wp_title( '&middot;', true, 'right' ); ?></title>

	<link rel="dns-prefetch" href="//oss.maxcdn.com">

	<!--[if lt IE 9]>
	<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="container">
	<div class="masthead">
	<h1 class="text-muted"><?php bloginfo( 'name' ); ?></h1>
	<?php wp_nav_menu( array(
		'theme_location' => 'header',
		'menu_class'     => 'nav nav-justified',
		'walker'         => new Library_Walker_Menu,
	) ); ?>
	</div>
</div>